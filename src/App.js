import React, {useState} from 'react';
import './App.css';

import GoalList from './components/GoalList/GoalList';
import './App.css'
import NewGoal from './components/NewGoal/NewGoal'

const App = () => {
  const [courseGoals, setCourseGoals] = useState([
    {id: 'cg1', text: '1'},
    {id: 'cg2', text: '2'},
    {id: 'cg3', text: '3'}
  ]);



  const addNewGoalHandler = newGoal => {
    // setCourseGoals(courseGoals.concat(newGoal));
    setCourseGoals((prevCourseGoals) => {
      return prevCourseGoals.concat(newGoal);

    });

  }

  return (
    <div className="course-goals">
      <h2>Course</h2>
      <NewGoal  onAddGoal={addNewGoalHandler}/>
      <GoalList goals={courseGoals}/>
    </div> 
  );
};
export default App;
